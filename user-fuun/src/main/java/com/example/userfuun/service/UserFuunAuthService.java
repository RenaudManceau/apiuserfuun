package com.example.userfuun.service;

import com.example.userfuun.entity.userfuun.UserFuun;
import com.example.userfuun.entity.userfuun.UserFuunAuthImp;
import com.example.userfuun.repository.UserFuunRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.transaction.Transactional;

public abstract class UserFuunAuthService implements UserDetailsService {
    final UserFuunRepository userFuunRepository;

    @Autowired
    public UserFuunAuthService(UserFuunRepository userFuunRepository)
    {
        this.userFuunRepository = userFuunRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException
    {

        UserFuun userFuun = userFuunRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("User not found with email: " + email));

        return UserFuunAuthImp.build(userFuun);
    }
}
